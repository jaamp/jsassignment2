var stringDate = '1/21/2019';
var addDays = 7;
var startDate = new Date(stringDate);
var dueDate = new Date(startDate);
dueDate.setDate(dueDate.getDate() + addDays);
var dueYear = dueDate.getFullYear().toString();
var dueMonth = (dueDate.getMonth() + 1).toString();
dueMonth = dueMonth.padStart(2,'0');
var longMonth = GetLongMonth(dueMonth);
var dueDay = dueDate.getDate().toString();
dueDay = dueDay.padStart(2,'0')
var tagDate = '<time datetime="' + dueYear +'-' + dueMonth + '-' + dueDay + '">' + longMonth + ' ' + dueDay + ', ' + dueYear + '</time>';

console.log(tagDate);

function GetLongMonth(dueMonth)
{
    switch (dueMonth)
    {
        case '01': return 'January';
        break;
        case '02': return 'February';
        break;
        case '03': return 'March';
        break;
        case '04': return 'April';
        break;
        case '05': return 'May';
        break;
        case '06': return 'June';
        break;
        case '07': return 'July';
        break;
        case '08': return 'August';
        break;
        case '09': return 'September';
        break;
        case '10': return 'October';
        break;
        case '11': return 'November';
        break;
        default : return 'December';
        break;
    }
}